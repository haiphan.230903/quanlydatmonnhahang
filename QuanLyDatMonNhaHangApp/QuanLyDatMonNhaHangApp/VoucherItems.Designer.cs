﻿namespace QuanLyDatMonNhaHangApp
{
    partial class VoucherItems
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vchPromocodeLabel = new System.Windows.Forms.Label();
            this.vchcodeLabel = new System.Windows.Forms.Label();
            this.vchCustomerIDLabel = new System.Windows.Forms.Label();
            this.vchOrderIDLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // vchPromocodeLabel
            // 
            this.vchPromocodeLabel.BackColor = System.Drawing.Color.Lime;
            this.vchPromocodeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vchPromocodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vchPromocodeLabel.Location = new System.Drawing.Point(16, 12);
            this.vchPromocodeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.vchPromocodeLabel.Name = "vchPromocodeLabel";
            this.vchPromocodeLabel.Size = new System.Drawing.Size(368, 237);
            this.vchPromocodeLabel.TabIndex = 9;
            this.vchPromocodeLabel.Text = "Mã khuyến mãi";
            this.vchPromocodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.vchPromocodeLabel.Click += new System.EventHandler(this.idLabel_Click);
            // 
            // vchcodeLabel
            // 
            this.vchcodeLabel.BackColor = System.Drawing.Color.Lime;
            this.vchcodeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vchcodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vchcodeLabel.Location = new System.Drawing.Point(396, 12);
            this.vchcodeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.vchcodeLabel.Name = "vchcodeLabel";
            this.vchcodeLabel.Size = new System.Drawing.Size(437, 237);
            this.vchcodeLabel.TabIndex = 10;
            this.vchcodeLabel.Text = "Mã voucher";
            this.vchcodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // vchCustomerIDLabel
            // 
            this.vchCustomerIDLabel.BackColor = System.Drawing.Color.Lime;
            this.vchCustomerIDLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vchCustomerIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vchCustomerIDLabel.Location = new System.Drawing.Point(845, 13);
            this.vchCustomerIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.vchCustomerIDLabel.Name = "vchCustomerIDLabel";
            this.vchCustomerIDLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.vchCustomerIDLabel.Size = new System.Drawing.Size(466, 238);
            this.vchCustomerIDLabel.TabIndex = 11;
            this.vchCustomerIDLabel.Text = "Mã khách hàng";
            this.vchCustomerIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.vchCustomerIDLabel.Click += new System.EventHandler(this.vchCustomerID_Click);
            // 
            // vchOrderIDLabel
            // 
            this.vchOrderIDLabel.BackColor = System.Drawing.Color.Lime;
            this.vchOrderIDLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vchOrderIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vchOrderIDLabel.Location = new System.Drawing.Point(1323, 13);
            this.vchOrderIDLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.vchOrderIDLabel.Name = "vchOrderIDLabel";
            this.vchOrderIDLabel.Size = new System.Drawing.Size(386, 237);
            this.vchOrderIDLabel.TabIndex = 12;
            this.vchOrderIDLabel.Text = "Mã đơn hàng";
            this.vchOrderIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VoucherItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cyan;
            this.Controls.Add(this.vchOrderIDLabel);
            this.Controls.Add(this.vchCustomerIDLabel);
            this.Controls.Add(this.vchcodeLabel);
            this.Controls.Add(this.vchPromocodeLabel);
            this.Name = "VoucherItems";
            this.Size = new System.Drawing.Size(1724, 263);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Label vchPromocodeLabel;
        public System.Windows.Forms.Label vchcodeLabel;
        public System.Windows.Forms.Label vchCustomerIDLabel;
        public System.Windows.Forms.Label vchOrderIDLabel;
    }
}

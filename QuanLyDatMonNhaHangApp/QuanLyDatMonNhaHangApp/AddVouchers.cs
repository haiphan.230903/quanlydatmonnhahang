﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace QuanLyDatMonNhaHangApp
{
    public partial class AddVouchers : Form
    {
        public AddVouchers()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Nhập voucher
            string vchPromocode = vchPromocodeTextBox.Text.Trim();
            string vchcode = vchcodeTextBox.Text.Trim();
            string vchCustomerID = vchCustomerIDTextBox.Text.Trim();
            string vchOrderID = vchOrderIDTextBox.Text.Trim();

            Program.voucherView.AddVoucher(new VoucherItems(vchPromocode, vchcode, vchCustomerID, vchOrderID));
            this.Close();
        }
    }
}

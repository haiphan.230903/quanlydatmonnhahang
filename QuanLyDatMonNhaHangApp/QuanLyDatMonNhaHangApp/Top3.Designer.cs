﻿namespace QuanLyDatMonNhaHangApp
{
    partial class Top3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.foodListFlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.foodItem3 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.foodItem2 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.foodItem1 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.foodListFlowLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // foodListFlowLayout
            // 
            this.foodListFlowLayout.AutoScroll = true;
            this.foodListFlowLayout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.foodListFlowLayout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodListFlowLayout.Controls.Add(this.foodItem3);
            this.foodListFlowLayout.Controls.Add(this.foodItem2);
            this.foodListFlowLayout.Controls.Add(this.foodItem1);
            this.foodListFlowLayout.Location = new System.Drawing.Point(15, 111);
            this.foodListFlowLayout.Margin = new System.Windows.Forms.Padding(6);
            this.foodListFlowLayout.Name = "foodListFlowLayout";
            this.foodListFlowLayout.Size = new System.Drawing.Size(1211, 687);
            this.foodListFlowLayout.TabIndex = 5;
            // 
            // foodItem3
            // 
            this.foodItem3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem3.Location = new System.Drawing.Point(12, 12);
            this.foodItem3.Margin = new System.Windows.Forms.Padding(12);
            this.foodItem3.Name = "foodItem3";
            this.foodItem3.Size = new System.Drawing.Size(1326, 189);
            this.foodItem3.TabIndex = 2;
            // 
            // foodItem2
            // 
            this.foodItem2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem2.Location = new System.Drawing.Point(12, 225);
            this.foodItem2.Margin = new System.Windows.Forms.Padding(12);
            this.foodItem2.Name = "foodItem2";
            this.foodItem2.Size = new System.Drawing.Size(1326, 189);
            this.foodItem2.TabIndex = 1;
            // 
            // foodItem1
            // 
            this.foodItem1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem1.Location = new System.Drawing.Point(12, 438);
            this.foodItem1.Margin = new System.Windows.Forms.Padding(12);
            this.foodItem1.Name = "foodItem1";
            this.foodItem1.Size = new System.Drawing.Size(1326, 189);
            this.foodItem1.TabIndex = 0;
            // 
            // Top3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 959);
            this.Controls.Add(this.foodListFlowLayout);
            this.Name = "Top3";
            this.Text = "Top3";
            this.Load += new System.EventHandler(this.Top3_Load);
            this.foodListFlowLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel foodListFlowLayout;
        private FoodItem foodItem3;
        private FoodItem foodItem2;
        private FoodItem foodItem1;
    }
}
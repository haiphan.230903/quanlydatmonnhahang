﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class FoodItem : UserControl
    {
        public FoodItem()
        {
            InitializeComponent();
        }

        public FoodItem(string id, Image pictureBox1, string name, string desc, string type, string point, string cost)
        {
            InitializeComponent();
            this.idLabel.Text = id;
            this.pictureBox1.Image = pictureBox1;
            this.nameLabel.Text = name;
            this.descLabel.Text = desc;
            this.typeLabel.Text = type;
            this.pointLabel.Text = point;
            this.costLabel.Text = cost;
        }

        private void nameLabel_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void modifyBtn_Click(object sender, EventArgs e)
        {
            ModifyFoodForm modifyForm = new ModifyFoodForm();
            modifyForm.Setup(this);
            modifyForm.Show();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            Program.formDataInfo.DeleteFood(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class OrderItem : UserControl
    {
        //string orderIDTest = "'O00000001'";
        string orderID;
        string strCon = @"Data Source=ADMIN;Initial Catalog=NewQLDMNH;Integrated Security=True";
        SqlConnection sqlCon = null;
        public OrderItem()
        {
            InitializeComponent();
            //LoadOrder();
        }

        public void LoadOrder(string orderID)
        {
            this.orderID = orderID;
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from Orders where OrderID = " + orderID;

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            while (reader.Read())
            {
                idLabel.Text = "ID: " + reader["OrderID"].ToString();
                statusLabel.Text = "Trạng thái: " + reader["Status"].ToString();
                timeLabel.Text = "Thời gian đặt: " + reader["OrderTime"].ToString();
                noteLabel.Text = "Ghi chú: " + reader["Note"].ToString();
                typeLabel.Text = "Loại: " + reader["OrderType"].ToString();
                totalPointLabel.Text = "Tổng điểm: " + reader["TotalPoint"].ToString();
                totalCostLabel.Text = "Tổng tiền: " + reader["TotalPrice"].ToString() + "đ";
            }
            reader.Close();

            LoadFoodsInOrder();

        }
        void LoadFoodsInOrder()
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from DishInOrder where OrderID = " + orderID;

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            
            orderFlowLayout.Controls.Clear();
            while (reader.Read())
            {
                string dishId = reader["DishID"].ToString();
                decimal count = Convert.ToDecimal(reader["NumberOfDish"]);

                FoodOrderItem foodOrderItem = new FoodOrderItem(dishId, count.ToString());
                orderFlowLayout.Controls.Add(foodOrderItem);
            }
            reader.Close();
        }

        private void OrderItem_Load(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

		private void totalCostLabel_Click(object sender, EventArgs e)
		{

		}

		private void RefreshBtn_Click(object sender, EventArgs e)
		{
            LoadOrder(orderID);
		}

		private void noteLabel_Click(object sender, EventArgs e)
		{

		}

		private void idLabel_Click(object sender, EventArgs e)
        {

        }
    }
}

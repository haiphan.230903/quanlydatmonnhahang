﻿namespace QuanLyDatMonNhaHangApp
{
    partial class OrderItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderFlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.idLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.noteLabel = new System.Windows.Forms.Label();
            this.typeLabel = new System.Windows.Forms.Label();
            this.totalCostLabel = new System.Windows.Forms.Label();
            this.totalPointLabel = new System.Windows.Forms.Label();
            this.foodOrderItem2 = new QuanLyDatMonNhaHangApp.FoodOrderItem();
            this.foodOrderItem1 = new QuanLyDatMonNhaHangApp.FoodOrderItem();
            this.foodOrderItem3 = new QuanLyDatMonNhaHangApp.FoodOrderItem();
            this.foodOrderItem4 = new QuanLyDatMonNhaHangApp.FoodOrderItem();
            this.orderFlowLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // orderFlowLayout
            // 
            this.orderFlowLayout.AutoScroll = true;
            this.orderFlowLayout.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.orderFlowLayout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderFlowLayout.Controls.Add(this.foodOrderItem2);
            this.orderFlowLayout.Controls.Add(this.foodOrderItem1);
            this.orderFlowLayout.Controls.Add(this.foodOrderItem3);
            this.orderFlowLayout.Controls.Add(this.foodOrderItem4);
            this.orderFlowLayout.Location = new System.Drawing.Point(21, 33);
            this.orderFlowLayout.Name = "orderFlowLayout";
            this.orderFlowLayout.Size = new System.Drawing.Size(483, 293);
            this.orderFlowLayout.TabIndex = 0;
            this.orderFlowLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // idLabel
            // 
            this.idLabel.Location = new System.Drawing.Point(22, 340);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(173, 18);
            this.idLabel.TabIndex = 7;
            this.idLabel.Text = "ID: 10";
            this.idLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.idLabel.Click += new System.EventHandler(this.idLabel_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(22, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "Hình ảnh";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(128, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(173, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Tên";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(273, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 23;
            this.label8.Text = "Điểm";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(338, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 14);
            this.label9.TabIndex = 24;
            this.label9.Text = "Giá";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(412, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 14);
            this.label1.TabIndex = 25;
            this.label1.Text = "Số lượng";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(22, 358);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(173, 20);
            this.statusLabel.TabIndex = 26;
            this.statusLabel.Text = "Trạng thái: đã giao";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timeLabel
            // 
            this.timeLabel.Location = new System.Drawing.Point(22, 379);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(173, 20);
            this.timeLabel.TabIndex = 27;
            this.timeLabel.Text = "Thời gian đặt: 10h 31/04/23";
            this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // noteLabel
            // 
            this.noteLabel.Location = new System.Drawing.Point(201, 340);
            this.noteLabel.Name = "noteLabel";
            this.noteLabel.Size = new System.Drawing.Size(141, 78);
            this.noteLabel.TabIndex = 28;
            this.noteLabel.Text = "Ghi chú: dálkdaslkd";
            this.noteLabel.Click += new System.EventHandler(this.noteLabel_Click);
            // 
            // typeLabel
            // 
            this.typeLabel.Location = new System.Drawing.Point(22, 398);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(173, 20);
            this.typeLabel.TabIndex = 29;
            this.typeLabel.Text = "Loại đơn: Đặt ship";
            this.typeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.typeLabel.Click += new System.EventHandler(this.label10_Click);
            // 
            // totalCostLabel
            // 
            this.totalCostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalCostLabel.Location = new System.Drawing.Point(348, 340);
            this.totalCostLabel.Name = "totalCostLabel";
            this.totalCostLabel.Size = new System.Drawing.Size(156, 23);
            this.totalCostLabel.TabIndex = 30;
            this.totalCostLabel.Text = "Thành tiền: 999$";
            this.totalCostLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.totalCostLabel.Click += new System.EventHandler(this.totalCostLabel_Click);
            // 
            // totalPointLabel
            // 
            this.totalPointLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPointLabel.Location = new System.Drawing.Point(348, 379);
            this.totalPointLabel.Name = "totalPointLabel";
            this.totalPointLabel.Size = new System.Drawing.Size(117, 30);
            this.totalPointLabel.TabIndex = 31;
            this.totalPointLabel.Text = "Tổng điểm: 99";
            this.totalPointLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // foodOrderItem2
            // 
            this.foodOrderItem2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.foodOrderItem2.Location = new System.Drawing.Point(3, 3);
            this.foodOrderItem2.Name = "foodOrderItem2";
            this.foodOrderItem2.Size = new System.Drawing.Size(458, 92);
            this.foodOrderItem2.TabIndex = 1;
            // 
            // foodOrderItem1
            // 
            this.foodOrderItem1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.foodOrderItem1.Location = new System.Drawing.Point(3, 101);
            this.foodOrderItem1.Name = "foodOrderItem1";
            this.foodOrderItem1.Size = new System.Drawing.Size(458, 92);
            this.foodOrderItem1.TabIndex = 0;
            // 
            // foodOrderItem3
            // 
            this.foodOrderItem3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.foodOrderItem3.Location = new System.Drawing.Point(3, 199);
            this.foodOrderItem3.Name = "foodOrderItem3";
            this.foodOrderItem3.Size = new System.Drawing.Size(458, 92);
            this.foodOrderItem3.TabIndex = 2;
            // 
            // foodOrderItem4
            // 
            this.foodOrderItem4.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.foodOrderItem4.Location = new System.Drawing.Point(3, 297);
            this.foodOrderItem4.Name = "foodOrderItem4";
            this.foodOrderItem4.Size = new System.Drawing.Size(458, 92);
            this.foodOrderItem4.TabIndex = 3;
            // 
            // OrderItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.totalPointLabel);
            this.Controls.Add(this.totalCostLabel);
            this.Controls.Add(this.typeLabel);
            this.Controls.Add(this.noteLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.idLabel);
            this.Controls.Add(this.orderFlowLayout);
            this.Name = "OrderItem";
            this.Size = new System.Drawing.Size(529, 432);
            this.Load += new System.EventHandler(this.OrderItem_Load);
            this.orderFlowLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel orderFlowLayout;
        private FoodOrderItem foodOrderItem1;
        private FoodOrderItem foodOrderItem2;
        private FoodOrderItem foodOrderItem3;
        private FoodOrderItem foodOrderItem4;
        private System.Windows.Forms.Label idLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label noteLabel;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.Label totalCostLabel;
        private System.Windows.Forms.Label totalPointLabel;
	}
}

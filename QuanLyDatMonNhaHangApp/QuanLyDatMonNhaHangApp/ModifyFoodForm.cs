﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class ModifyFoodForm : Form
    {
        public ModifyFoodForm()
        {
            InitializeComponent();
        }

        public void Setup(FoodItem foodItem)
        {
            idTextBox.Text = foodItem.idLabel.Text;
            nameTextBox.Text = foodItem.nameLabel.Text;
            descTextBox.Text = foodItem.descLabel.Text;
            typeTextBox.Text = foodItem.typeLabel.Text;
            pointTextBox.Text = foodItem.pointLabel.Text;
            costTextBox.Text = foodItem.costLabel.Text;
            pictureBox1.Image = foodItem.pictureBox1.Image;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";
                openFileDialog.Title = "Chọn một bức ảnh";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        // Lấy đường dẫn của tập tin đã chọn
                        string selectedFilePath = openFileDialog.FileName;

                        // Hiển thị ảnh trong PictureBox (thay thế "pictureBox1" bằng tên PictureBox của bạn)
                        pictureBox1.Image = Image.FromFile(selectedFilePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Đã xảy ra lỗi khi tải ảnh: " + ex.Message);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Xử lí đồng ý nhập món
            string id = idTextBox.Text.Trim();
            string name = nameTextBox.Text.Trim();
            string desc = descTextBox.Text.Trim();
            string type = typeTextBox.Text.Trim();
            string point = pointTextBox.Text.Trim();
            string cost = costTextBox.Text.Trim();
            Image image = pictureBox1.Image;
            Program.formDataInfo.ModifyFood(new FoodItem(id, image, name, desc, type, point, cost));
            this.Close();
        }
    }
}

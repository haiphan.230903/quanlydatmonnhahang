﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class FormDataInfo : Form
    {
        string strCon = @"Data Source=ADMIN;Initial Catalog=NewQLDMNH;Integrated Security=True";
        SqlConnection sqlCon = null;

        public FormDataInfo()
        {
            InitializeComponent();
        }

        private void btnMoKetNoi_Click(object sender, EventArgs e)
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDongKetNoi_Click(object sender, EventArgs e)
        {
            if (sqlCon != null && sqlCon.State == ConnectionState.Open)
            {
                sqlCon.Close();
                MessageBox.Show("Dong ket noi thanh cong");
            }
            else
            {
                MessageBox.Show("Chua tao ket noi");
            }
        }

        private void FormDataInfo_Load(object sender, EventArgs e)
        {
            LoadFoodList();
            comboBox1.Text = "Mã số";
            comboBox1.Text = "Giá tiền";
        }

        void LoadFoodList()
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from Dish";

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            foodListFlowLayout.Controls.Clear();
            while (reader.Read())
            {
                string courseId = reader["DishID"].ToString();
                string name = reader["Name"].ToString();
                string description = reader["Description"].ToString();
                string type = reader["Type"].ToString();
                string price = reader["Price"].ToString();
                decimal point = Convert.ToDecimal(reader["Point"]);

                Image image = null;
                try
                {
                    // Retrieve image data as byte array
                    byte[] imageData = (byte[])reader["Picture"];

                    // Convert byte array to Image              
                    using (MemoryStream ms = new MemoryStream(imageData))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                catch
                {
                    //chõ này là ảnh bị gì đó
                }

                foodListFlowLayout.Controls.Add(new FoodItem(courseId, image, name, description, type, point.ToString(), price));
            }
            reader.Close();
        }

        public void AddFood(FoodItem foodItem)
        {
            //SqlCommand sqlCmd = new SqlCommand();
            //sqlCmd.CommandType = CommandType.Text;

            //try
            //{
            //    // Convert the Image to a byte array
            //    byte[] imageBytes;
            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        foodItem.pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg); // Change the format based on your image format
            //        imageBytes = ms.ToArray();
            //    }
            //    sqlCmd.CommandText = "INSERT INTO Dish (DishID, Name, Description, Type, Price, Point, Picture) VALUES (@DishID, @Name, @Description, @Type, @Price, @Point, @Picture)";
            //    sqlCmd.Parameters.AddWithValue("@Picture", imageBytes);
            //}
            //catch
            //{
            //    sqlCmd.CommandText = "INSERT INTO Dish (DishID, Name, Description, Type, Price, Point) VALUES (@DishID, @Name, @Description, @Type, @Price, @Point)";
            //}
            SqlCommand sqlCmd = new SqlCommand("InsertDish", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.AddWithValue("@DishID", foodItem.idLabel.Text);
            sqlCmd.Parameters.AddWithValue("@Name", foodItem.nameLabel.Text);
            sqlCmd.Parameters.AddWithValue("@Description", foodItem.descLabel.Text);
            sqlCmd.Parameters.AddWithValue("@Type", foodItem.typeLabel.Text);
            sqlCmd.Parameters.AddWithValue("@Price", foodItem.costLabel.Text);
            sqlCmd.Parameters.AddWithValue("@Point", foodItem.pointLabel.Text);
            try
            {
                byte[] imageBytes;
                using (MemoryStream ms = new MemoryStream())
                {
                    foodItem.pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg); // Change the format based on your image format
                    imageBytes = ms.ToArray();
                }
                sqlCmd.Parameters.AddWithValue("@Picture", imageBytes);
            }
            catch (Exception ex)
            {
                sqlCmd.Parameters.AddWithValue("@Picture", DBNull.Value);
            }


            sqlCmd.Connection = sqlCon;
            try
            {
                sqlCmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                foreach (SqlError error in ex.Errors)
                {
                    if (error.Number == 3621 || error.Number == 2627)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Có khóa chính bị trùng!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    if (error.Number == 8114)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Có thuộc tính bị nhập sai kiểu dữ liệu!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    if (error.Number == 8115)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Dự liệu bị tràn so với kiểu dữ liệu!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    if (error.Number == 2601)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Không được có giá trị trùng lặp vào một cột có ràng buộc duy nhất hoặc khóa chính!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    break;
                }
            }

            LoadFoodList();
        }
        public void ModifyFood(FoodItem foodItem)
        {
            //SqlCommand sqlCmd = new SqlCommand();
            //sqlCmd.CommandType = CommandType.Text;

            //try
            //{
            //    // Convert the Image to a byte array
            //    byte[] imageBytes;
            //    using (MemoryStream ms = new MemoryStream())
            //    {
            //        foodItem.pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg); // Change the format based on your image format
            //        imageBytes = ms.ToArray();
            //    }
            //    sqlCmd.CommandText = "UPDATE Dish SET Picture = @Picture WHERE DishID = @DishID";
            //    sqlCmd.Parameters.AddWithValue("@DishID", foodItem.idLabel.Text);
            //    sqlCmd.Parameters.AddWithValue("@Picture", imageBytes);
            //    sqlCmd.Connection = sqlCon;

            //    sqlCmd.ExecuteNonQuery();
            //}
            //catch
            //{
            //    //Chỗ này dành cho trường hợp không chọn ảnh hoặc không thay đổi ảnh, có thể không cần làm j ở đây cả
            //    return;
            //}

            //sqlCmd = new SqlCommand();
            //sqlCmd.CommandType = CommandType.Text;

            SqlCommand sqlCmd = new SqlCommand("UpdateDish", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            // Sử dụng tham số để tránh SQL Injection
            //sqlCmd.CommandText = "UPDATE Dish SET Name = @Name, Description = @Description, Type = @Type, Price = @Price, Point = @Point WHERE DishID = @DishID";

            // Thêm tham số

            sqlCmd.Parameters.AddWithValue("@DishID", foodItem.idLabel.Text);
            sqlCmd.Parameters.AddWithValue("@NewName", foodItem.nameLabel.Text);
            sqlCmd.Parameters.AddWithValue("@NewDescription", foodItem.descLabel.Text);
            sqlCmd.Parameters.AddWithValue("@NewType", foodItem.typeLabel.Text);
            sqlCmd.Parameters.AddWithValue("@NewPrice", foodItem.costLabel.Text);
            sqlCmd.Parameters.AddWithValue("@NewPoint", foodItem.pointLabel.Text);

            try
            {
                byte[] imageBytes;
                using (MemoryStream ms = new MemoryStream())
                {
                    foodItem.pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg); // Change the format based on your image format
                    imageBytes = ms.ToArray();
                }
                sqlCmd.Parameters.AddWithValue("@NewPicture", imageBytes);
            }
            catch (Exception ex)
            {
                AnnounceForm announceForm = new AnnounceForm();
                announceForm.annouceText.Text = "Lỗi: Ảnh lỗi";
                announceForm.Show();
                announceForm.BringToFront();
                sqlCmd.Parameters.AddWithValue("@NewPicture", DBNull.Value);

            }

            sqlCmd.Connection = sqlCon;
            try
            {
                sqlCmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                foreach (SqlError error in ex.Errors)
                {
                    if (error.Number == 8114)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Có thuộc tính bị nhập sai kiểu dữ liệu!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    if (error.Number == 8115)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Dự liệu bị tràn so với kiểu dữ liệu!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    if (error.Number == 2601)
                    {
                        AnnounceForm announceForm = new AnnounceForm();
                        announceForm.annouceText.Text = "Lỗi: Không được có giá trị trùng lặp vào một cột có ràng buộc duy nhất hoặc khóa chính!";
                        announceForm.Show();
                        announceForm.BringToFront();
                    }
                    // Xử lý các lỗi khác nếu cần
                    AnnounceForm announceForm2 = new AnnounceForm();
                    announceForm2.annouceText.Text = ex.Message;
                    announceForm2.Show();
                    announceForm2.BringToFront();
                    break;
                }
            }
            LoadFoodList();
        }
        public void DeleteFood(FoodItem foodItem)
        {
            try
            {
                using (SqlCommand sqlCmd = new SqlCommand("DeleteDish", sqlCon))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@DishID", foodItem.idLabel.Text);
                    sqlCmd.ExecuteNonQuery();
                }
                LoadFoodList();
            }
            catch (Exception ex)
            {
                AnnounceForm announceForm = new AnnounceForm();
                announceForm.annouceText.Text = ex.Message;
                announceForm.Show();
                announceForm.BringToFront();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AddFoodForm addFoodForm = new AddFoodForm();
            addFoodForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VoucherView voucherView = new VoucherView();
            voucherView.Show();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void foodListFlowLayout_Paint(object sender, PaintEventArgs e)
        {

        }

        private void formNameLabel_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            if (comboBox1.Text == "Id")
            {
                sqlCmd.CommandText = "SELECT * FROM Dish ORDER BY DishID ASC";
            }
            else
            {
                sqlCmd.CommandText = "SELECT * FROM Dish ORDER BY Price ASC";
            }

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            foodListFlowLayout.Controls.Clear();
            while (reader.Read())
            {
                string courseId = reader["DishID"].ToString();
                string name = reader["Name"].ToString();
                string description = reader["Description"].ToString();
                string type = reader["Type"].ToString();
                string price = reader["Price"].ToString();
                decimal point = Convert.ToDecimal(reader["Point"]);

                Image image = null;
                try
                {
                    // Retrieve image data as byte array
                    byte[] imageData = (byte[])reader["Picture"];

                    // Convert byte array to Image              
                    using (MemoryStream ms = new MemoryStream(imageData))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                catch
                {
                    //chõ này là ảnh bị gì đó
                }

                foodListFlowLayout.Controls.Add(new FoodItem(courseId, image, name, description, type, point.ToString(), price));
            }
            reader.Close();
        }

        private void textBox1_TextChanged(FoodItem foodItem)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand sqlCmd = new SqlCommand("SearchFoodByName", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            //SqlCommand sqlCmd = new SqlCommand();
            //sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "SELECT * FROM Dish WHERE Name LIKE @searchTerm";
            sqlCmd.Parameters.AddWithValue("@FoodName", textBox1.Text);

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            foodListFlowLayout.Controls.Clear();
            while (reader.Read())
            {
                string courseId = reader["DishID"].ToString();
                string name = reader["Name"].ToString();
                string description = reader["Description"].ToString();
                string type = reader["Type"].ToString();
                string price = reader["Price"].ToString();
                decimal point = Convert.ToDecimal(reader["Point"]);

                Image image = null;
                try
                {
                    // Retrieve image data as byte array
                    byte[] imageData = (byte[])reader["Picture"];

                    // Convert byte array to Image              
                    using (MemoryStream ms = new MemoryStream(imageData))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                catch
                {
                    //chõ này là ảnh bị gì đó
                }

                foodListFlowLayout.Controls.Add(new FoodItem(courseId, image, name, description, type, point.ToString(), price));
            }
            reader.Close();
        }


        private void foodItem3_Load(object sender, EventArgs e)
        {

        }

        private void OrderBtn_Click(object sender, EventArgs e)
        {
            Orders orders = new Orders();
            orders.Show();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Sushimax sushimax = new Sushimax();
            sushimax.Show();
        }

        private void button1_Click_3(object sender, EventArgs e)
        {

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            ////sqlCmd.CommandText = "SELECT d.*\r\nFROM Dish d\r\nWHERE d.DishID IN (\r\n    SELECT tf.DishID\r\n    FROM dbo.Top3FavoriteFood('2023-01-01', '2023-12-31') tf -- Adjust date range as needed\r\n))";
            sqlCmd.CommandText = "select * from dish where DishID IN ((select DishID from Top3FavoriteFood('" + startDayBox.Text +"','" + endDayBox.Text +"')));";

            sqlCmd.Connection = sqlCon;

            try
            {
                SqlDataReader reader = sqlCmd.ExecuteReader();

                foodListFlowLayout.Controls.Clear();
                while (reader.Read())
                {
                    string courseId = reader["DishID"].ToString();
                    string name = reader["Name"].ToString();
                    string description = reader["Description"].ToString();
                    string type = reader["Type"].ToString();
                    string price = reader["Price"].ToString();
                    decimal point = Convert.ToDecimal(reader["Point"]);

                    Image image = null;
                    try
                    {
                        // Retrieve image data as byte array
                        byte[] imageData = (byte[])reader["Picture"];

                        // Convert byte array to Image              
                        using (MemoryStream ms = new MemoryStream(imageData))
                        {
                            image = Image.FromStream(ms);
                        }
                    }
                    catch
                    {

                    }

                    foodListFlowLayout.Controls.Add(new FoodItem(courseId, image, name, description, type, point.ToString(), price));
                }
                reader.Close();
            }
            catch
            {
                AnnounceForm announceForm = new AnnounceForm();
                announceForm.annouceText.Text = "Lỗi: Ngày nhập sai định dạng!";
                announceForm.Show();
                announceForm.BringToFront();
                return;
            }

        }
    }
}

﻿namespace QuanLyDatMonNhaHangApp
{
    partial class VoucherView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vchViewLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.voucherItems1 = new QuanLyDatMonNhaHangApp.VoucherItems();
            this.voucherItems2 = new QuanLyDatMonNhaHangApp.VoucherItems();
            this.voucherItems3 = new QuanLyDatMonNhaHangApp.VoucherItems();
            this.voucherItems4 = new QuanLyDatMonNhaHangApp.VoucherItems();
            this.vchViewLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // vchViewLayoutPanel1
            // 
            this.vchViewLayoutPanel1.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.vchViewLayoutPanel1.AutoScroll = true;
            this.vchViewLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.vchViewLayoutPanel1.Controls.Add(this.voucherItems1);
            this.vchViewLayoutPanel1.Controls.Add(this.voucherItems2);
            this.vchViewLayoutPanel1.Controls.Add(this.voucherItems3);
            this.vchViewLayoutPanel1.Controls.Add(this.voucherItems4);
            this.vchViewLayoutPanel1.Location = new System.Drawing.Point(156, 84);
            this.vchViewLayoutPanel1.Name = "vchViewLayoutPanel1";
            this.vchViewLayoutPanel1.Size = new System.Drawing.Size(1760, 912);
            this.vchViewLayoutPanel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Fuchsia;
            this.button1.Location = new System.Drawing.Point(12, 894);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 93);
            this.button1.TabIndex = 2;
            this.button1.Text = "Thêm Voucher";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 39);
            this.label1.TabIndex = 3;
            this.label1.Text = "Voucher có sẵn";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Mới nhất",
            "Cũ nhất"});
            this.comboBox1.Location = new System.Drawing.Point(282, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(265, 33);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.Visible = false;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(276, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 31);
            this.label2.TabIndex = 5;
            this.label2.Text = "Mã khuyến mãi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(689, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 31);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mã voucher";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1128, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 31);
            this.label4.TabIndex = 7;
            this.label4.Text = "Mã khách hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1631, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 31);
            this.label5.TabIndex = 8;
            this.label5.Text = "Mã đơn";
            // 
            // voucherItems1
            // 
            this.voucherItems1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.voucherItems1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.voucherItems1.Location = new System.Drawing.Point(3, 3);
            this.voucherItems1.Name = "voucherItems1";
            this.voucherItems1.Size = new System.Drawing.Size(1719, 263);
            this.voucherItems1.TabIndex = 2;
            // 
            // voucherItems2
            // 
            this.voucherItems2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.voucherItems2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.voucherItems2.Location = new System.Drawing.Point(3, 272);
            this.voucherItems2.Name = "voucherItems2";
            this.voucherItems2.Size = new System.Drawing.Size(1719, 263);
            this.voucherItems2.TabIndex = 1;
            // 
            // voucherItems3
            // 
            this.voucherItems3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.voucherItems3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.voucherItems3.Location = new System.Drawing.Point(3, 541);
            this.voucherItems3.Name = "voucherItems3";
            this.voucherItems3.Size = new System.Drawing.Size(1719, 263);
            this.voucherItems3.TabIndex = 2;
            // 
            // voucherItems4
            // 
            this.voucherItems4.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.voucherItems4.Location = new System.Drawing.Point(3, 810);
            this.voucherItems4.Name = "voucherItems4";
            this.voucherItems4.Size = new System.Drawing.Size(1719, 263);
            this.voucherItems4.TabIndex = 3;
            // 
            // VoucherView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1949, 1008);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.vchViewLayoutPanel1);
            this.Name = "VoucherView";
            this.Text = "VoucherView";
            this.Load += new System.EventHandler(this.VoucherView_Load_1);
            this.vchViewLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel vchViewLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private VoucherItems voucherItems1;
        private VoucherItems voucherItems2;
        private VoucherItems voucherItems3;
        private VoucherItems voucherItems4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}
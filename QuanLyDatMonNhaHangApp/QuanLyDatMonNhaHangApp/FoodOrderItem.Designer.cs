﻿namespace QuanLyDatMonNhaHangApp
{
    partial class FoodOrderItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.idLabel = new System.Windows.Forms.Label();
			this.costLabel = new System.Windows.Forms.Label();
			this.pointLabel = new System.Windows.Forms.Label();
			this.nameLabel = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.countLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// idLabel
			// 
			this.idLabel.BackColor = System.Drawing.SystemColors.Control;
			this.idLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.idLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.idLabel.Location = new System.Drawing.Point(98, 9);
			this.idLabel.Name = "idLabel";
			this.idLabel.Size = new System.Drawing.Size(50, 77);
			this.idLabel.TabIndex = 20;
			this.idLabel.Text = "ID";
			this.idLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// costLabel
			// 
			this.costLabel.BackColor = System.Drawing.SystemColors.Control;
			this.costLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.costLabel.Location = new System.Drawing.Point(316, 9);
			this.costLabel.Name = "costLabel";
			this.costLabel.Size = new System.Drawing.Size(64, 76);
			this.costLabel.TabIndex = 19;
			this.costLabel.Text = "9999999$";
			this.costLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pointLabel
			// 
			this.pointLabel.BackColor = System.Drawing.SystemColors.Control;
			this.pointLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pointLabel.Location = new System.Drawing.Point(250, 9);
			this.pointLabel.Name = "pointLabel";
			this.pointLabel.Size = new System.Drawing.Size(60, 76);
			this.pointLabel.TabIndex = 18;
			this.pointLabel.Text = "Điểm";
			this.pointLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// nameLabel
			// 
			this.nameLabel.BackColor = System.Drawing.SystemColors.Control;
			this.nameLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nameLabel.Location = new System.Drawing.Point(154, 9);
			this.nameLabel.Name = "nameLabel";
			this.nameLabel.Size = new System.Drawing.Size(90, 77);
			this.nameLabel.TabIndex = 17;
			this.nameLabel.Text = "KFC";
			this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::QuanLyDatMonNhaHangApp.Properties.Resources.FallingShapes;
			this.pictureBox1.Location = new System.Drawing.Point(9, 9);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(83, 77);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 16;
			this.pictureBox1.TabStop = false;
			// 
			// countLabel
			// 
			this.countLabel.BackColor = System.Drawing.SystemColors.Control;
			this.countLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.countLabel.Location = new System.Drawing.Point(386, 8);
			this.countLabel.Name = "countLabel";
			this.countLabel.Size = new System.Drawing.Size(64, 76);
			this.countLabel.TabIndex = 21;
			this.countLabel.Text = "Số lượng";
			this.countLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// FoodOrderItem
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.Controls.Add(this.countLabel);
			this.Controls.Add(this.idLabel);
			this.Controls.Add(this.costLabel);
			this.Controls.Add(this.pointLabel);
			this.Controls.Add(this.nameLabel);
			this.Controls.Add(this.pictureBox1);
			this.Name = "FoodOrderItem";
			this.Size = new System.Drawing.Size(458, 92);
			this.Load += new System.EventHandler(this.FoodOrderItem_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label idLabel;
        public System.Windows.Forms.Label costLabel;
        public System.Windows.Forms.Label pointLabel;
        public System.Windows.Forms.Label nameLabel;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label countLabel;
    }
}

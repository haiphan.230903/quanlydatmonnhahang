﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class Sushimax : Form
    {
        string strCon = @"Data Source=DESKTOP-RUC0G3P\PHUQUOC;Initial Catalog=restau_btl_final;Integrated Security=True";
        SqlConnection sqlCon = null;
        public Sushimax()
        {
            InitializeComponent();
            LoadSushiMax();
        }

        public void LoadSushiMax()
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from Orders where Status ='Completed'";

            sqlCmd.Connection = sqlCon;
            SqlDataReader reader = sqlCmd.ExecuteReader();
            sushimaxlayout.Controls.Clear();
            while (reader.Read())
            {
                string orderId = reader["OrderID"].ToString();
                OrderItem orderItem = new OrderItem();
                orderItem.LoadOrder("'" + orderId + "'");
                sushimaxlayout.Controls.Add(orderItem);
            }
            reader.Close();
        }

        private void sushimaxlayout_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

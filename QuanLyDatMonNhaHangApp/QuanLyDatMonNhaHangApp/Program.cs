﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static public FormDataInfo formDataInfo;
        static public VoucherView voucherView;
        [STAThread]
        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            formDataInfo = new FormDataInfo();
            voucherView = new VoucherView();
            Application.Run(formDataInfo);
        }
    }
}

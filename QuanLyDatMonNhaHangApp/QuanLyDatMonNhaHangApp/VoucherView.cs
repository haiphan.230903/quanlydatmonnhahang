﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace QuanLyDatMonNhaHangApp
{
    public partial class VoucherView : Form
    {
        string strCon = @"Data Source=ADMIN;Initial Catalog=NewQLDMNH;Integrated Security=True";
        SqlConnection sqlCon = null;

        public VoucherView()
        {
            InitializeComponent();
            LoadVchList();
        }


        private void btnMoKetNoi_Click(object sender, EventArgs e)
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDongKetNoi_Click(object sender, EventArgs e)
        {
            if (sqlCon != null && sqlCon.State == ConnectionState.Open)
            {
                sqlCon.Close();
                MessageBox.Show("Dong ket noi thanh cong");
            }
            else
            {
                MessageBox.Show("Chua tao ket noi");
            }
        }

        private void VoucherView_Load(object sender, EventArgs e)
        {
            LoadVchList();
            comboBox1.Text = "Mới nhất";

        }

        void LoadVchList()
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from Voucher";

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            vchViewLayoutPanel1.Controls.Clear();
            while (reader.Read())
            {
                string vchPromocode = reader["PromotionCode"].ToString();
                string vchcode = reader["VoucherCode"].ToString();
                string vchCustomerID = reader["CustomerID"].ToString();
                string vchOrderID = reader["OrderID"].ToString();
                vchViewLayoutPanel1.Controls.Add(new VoucherItems(vchPromocode, vchcode, vchCustomerID, vchOrderID));
            }
            reader.Close();
        }

        public void AddVoucher(VoucherItems voucherItems)
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    MessageBox.Show("Them voucher thanh cong!");
                }
                else
                {
                    MessageBox.Show("Chua tao ket noi");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;


            // Sử dụng tham số để tránh SQL Injection
            sqlCmd.CommandText = "INSERT INTO Voucher (PromotionCode, VoucherCode, CustomerID, OrderID) VALUES (@PromotionCode, @VoucherCode, @CustomerID, @OrderID)";

            // Thêm tham số
            sqlCmd.Parameters.AddWithValue("@PromotionCode", voucherItems.vchPromocodeLabel.Text);
            sqlCmd.Parameters.AddWithValue("@VoucherCode", voucherItems.vchcodeLabel.Text);
            sqlCmd.Parameters.AddWithValue("@CustomerID", voucherItems.vchCustomerIDLabel.Text);
            sqlCmd.Parameters.AddWithValue("@OrderID", voucherItems.vchOrderIDLabel.Text);


            sqlCmd.Connection = sqlCon;

            // Mở kết nối và thực hiện lệnh SQL
            sqlCmd.ExecuteNonQuery();

            // Sau khi thêm mới, cập nhật danh sách
            LoadVchList();
        }
        private void vchbtn_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddVouchers addVouchers = new AddVouchers();
            addVouchers.Show();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void VoucherView_Load_1(object sender, EventArgs e)
        {

        }
    }
}

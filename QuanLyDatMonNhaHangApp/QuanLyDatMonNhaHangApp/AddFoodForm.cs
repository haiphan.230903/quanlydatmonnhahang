﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace QuanLyDatMonNhaHangApp
{
    public partial class AddFoodForm : Form
    {
        public AddFoodForm()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //Xử lí đồng ý nhập món
            string id = idTextBox.Text.Trim();
            string name = nameTextBox.Text.Trim();
            string desc = descTextBox.Text.Trim();
            string type = typeTextBox.Text.Trim();
            string point = pointTextBox.Text.Trim();
            string cost = costTextBox.Text.Trim();
            Image image = pictureBox1.Image;
            Program.formDataInfo.AddFood(new FoodItem(id, image, name, desc, type, point, cost));
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";
                openFileDialog.Title = "Chọn một bức ảnh";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        // Lấy đường dẫn của tập tin đã chọn
                        string selectedFilePath = openFileDialog.FileName;

                        // Hiển thị ảnh trong PictureBox (thay thế "pictureBox1" bằng tên PictureBox của bạn)
                        pictureBox1.Image = Image.FromFile(selectedFilePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Đã xảy ra lỗi khi tải ảnh: " + ex.Message);
                    }
                }
            }
        }
    }
}

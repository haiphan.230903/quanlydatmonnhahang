﻿namespace QuanLyDatMonNhaHangApp
{
    partial class Orders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderListFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // orderListFlow
            // 
            this.orderListFlow.AutoScroll = true;
            this.orderListFlow.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.orderListFlow.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.orderListFlow.Location = new System.Drawing.Point(25, 12);
            this.orderListFlow.Name = "orderListFlow";
            this.orderListFlow.Size = new System.Drawing.Size(536, 470);
            this.orderListFlow.TabIndex = 5;
            this.orderListFlow.Paint += new System.Windows.Forms.PaintEventHandler(this.orderListFlow_Paint);
            // 
            // Orders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 500);
            this.Controls.Add(this.orderListFlow);
            this.Name = "Orders";
            this.Text = "Orders";
            this.Load += new System.EventHandler(this.Orders_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel orderListFlow;
    }
}
﻿namespace QuanLyDatMonNhaHangApp
{
    partial class FormDataInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.addFoodBtn = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.foodListFlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.foodItem3 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.foodItem2 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.foodItem1 = new QuanLyDatMonNhaHangApp.FoodItem();
            this.formNameLabel = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.seachbtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.OrderBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.formDataInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.startDayBox = new System.Windows.Forms.TextBox();
            this.endDayBox = new System.Windows.Forms.TextBox();
            this.foodListFlowLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formDataInfoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // addFoodBtn
            // 
            this.addFoodBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.addFoodBtn.Location = new System.Drawing.Point(724, 463);
            this.addFoodBtn.Name = "addFoodBtn";
            this.addFoodBtn.Size = new System.Drawing.Size(75, 23);
            this.addFoodBtn.TabIndex = 3;
            this.addFoodBtn.Text = "Thêm món";
            this.addFoodBtn.UseVisualStyleBackColor = false;
            this.addFoodBtn.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // foodListFlowLayout
            // 
            this.foodListFlowLayout.AutoScroll = true;
            this.foodListFlowLayout.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.foodListFlowLayout.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodListFlowLayout.Controls.Add(this.foodItem3);
            this.foodListFlowLayout.Controls.Add(this.foodItem2);
            this.foodListFlowLayout.Controls.Add(this.foodItem1);
            this.foodListFlowLayout.Location = new System.Drawing.Point(99, 123);
            this.foodListFlowLayout.Name = "foodListFlowLayout";
            this.foodListFlowLayout.Size = new System.Drawing.Size(700, 325);
            this.foodListFlowLayout.TabIndex = 4;
            this.foodListFlowLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.foodListFlowLayout_Paint);
            // 
            // foodItem3
            // 
            this.foodItem3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem3.Location = new System.Drawing.Point(6, 6);
            this.foodItem3.Margin = new System.Windows.Forms.Padding(6);
            this.foodItem3.Name = "foodItem3";
            this.foodItem3.Size = new System.Drawing.Size(665, 100);
            this.foodItem3.TabIndex = 2;
            this.foodItem3.Load += new System.EventHandler(this.foodItem3_Load);
            // 
            // foodItem2
            // 
            this.foodItem2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem2.Location = new System.Drawing.Point(6, 118);
            this.foodItem2.Margin = new System.Windows.Forms.Padding(6);
            this.foodItem2.Name = "foodItem2";
            this.foodItem2.Size = new System.Drawing.Size(665, 100);
            this.foodItem2.TabIndex = 1;
            // 
            // foodItem1
            // 
            this.foodItem1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.foodItem1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.foodItem1.Location = new System.Drawing.Point(6, 230);
            this.foodItem1.Margin = new System.Windows.Forms.Padding(6);
            this.foodItem1.Name = "foodItem1";
            this.foodItem1.Size = new System.Drawing.Size(665, 100);
            this.foodItem1.TabIndex = 0;
            // 
            // formNameLabel
            // 
            this.formNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formNameLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.formNameLabel.Location = new System.Drawing.Point(25, 22);
            this.formNameLabel.Name = "formNameLabel";
            this.formNameLabel.Size = new System.Drawing.Size(214, 25);
            this.formNameLabel.TabIndex = 5;
            this.formNameLabel.Text = "Danh sách món ăn";
            this.formNameLabel.Click += new System.EventHandler(this.formNameLabel_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Id",
            "Price"});
            this.comboBox1.Location = new System.Drawing.Point(99, 65);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(134, 21);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Sắp xếp theo:";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(320, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tên:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(402, 66);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(348, 20);
            this.textBox1.TabIndex = 9;
            // 
            // seachbtn
            // 
            this.seachbtn.Location = new System.Drawing.Point(752, 65);
            this.seachbtn.Margin = new System.Windows.Forms.Padding(2);
            this.seachbtn.Name = "seachbtn";
            this.seachbtn.Size = new System.Drawing.Size(52, 18);
            this.seachbtn.TabIndex = 10;
            this.seachbtn.Text = "Tìm kiếm";
            this.seachbtn.UseVisualStyleBackColor = true;
            this.seachbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(99, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 14);
            this.label3.TabIndex = 11;
            this.label3.Text = "Hình ảnh";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(216, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(276, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 14);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tên";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(370, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "Mô tả";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(485, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Loại";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(567, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "Điểm";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(632, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "Giá";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button2.Location = new System.Drawing.Point(23, 525);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 21);
            this.button2.TabIndex = 18;
            this.button2.Text = "Vouchers";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // OrderBtn
            // 
            this.OrderBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OrderBtn.Location = new System.Drawing.Point(107, 466);
            this.OrderBtn.Margin = new System.Windows.Forms.Padding(2);
            this.OrderBtn.Name = "OrderBtn";
            this.OrderBtn.Size = new System.Drawing.Size(132, 21);
            this.OrderBtn.TabIndex = 19;
            this.OrderBtn.Text = "Order done with Sushi";
            this.OrderBtn.UseVisualStyleBackColor = false;
            this.OrderBtn.Click += new System.EventHandler(this.OrderBtn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Yellow;
            this.button1.Location = new System.Drawing.Point(256, 466);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 21);
            this.button1.TabIndex = 20;
            this.button1.Text = "Top 3";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // formDataInfoBindingSource
            // 
            this.formDataInfoBindingSource.DataSource = typeof(QuanLyDatMonNhaHangApp.FormDataInfo);
            // 
            // startDayBox
            // 
            this.startDayBox.Location = new System.Drawing.Point(349, 467);
            this.startDayBox.Margin = new System.Windows.Forms.Padding(2);
            this.startDayBox.Name = "startDayBox";
            this.startDayBox.Size = new System.Drawing.Size(96, 20);
            this.startDayBox.TabIndex = 21;
            this.startDayBox.Text = "YYYY-MM-DD";
            // 
            // endDayBox
            // 
            this.endDayBox.Location = new System.Drawing.Point(460, 467);
            this.endDayBox.Margin = new System.Windows.Forms.Padding(2);
            this.endDayBox.Name = "endDayBox";
            this.endDayBox.Size = new System.Drawing.Size(96, 20);
            this.endDayBox.TabIndex = 22;
            this.endDayBox.Text = "YYYY-MM-DD";
            // 
            // FormDataInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(880, 507);
            this.Controls.Add(this.endDayBox);
            this.Controls.Add(this.startDayBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.OrderBtn);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.seachbtn);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.formNameLabel);
            this.Controls.Add(this.addFoodBtn);
            this.Controls.Add(this.foodListFlowLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormDataInfo";
            this.Text = "FormDataInfo";
            this.Load += new System.EventHandler(this.FormDataInfo_Load);
            this.foodListFlowLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.formDataInfoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button addFoodBtn;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.FlowLayoutPanel foodListFlowLayout;
        private FoodItem foodItem2;
        private FoodItem foodItem1;
        private FoodItem foodItem3;
        private System.Windows.Forms.Label formNameLabel;
        private System.Windows.Forms.BindingSource formDataInfoBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button seachbtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        internal System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button OrderBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox startDayBox;
        private System.Windows.Forms.TextBox endDayBox;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class FoodOrderItem : UserControl
    {
        string strCon = @"Data Source=ADMIN;Initial Catalog=NewQLDMNH;Integrated Security=True";
        SqlConnection sqlCon = null;
        public decimal price, point;
        string name;
        Image image = null;
        public FoodOrderItem()
        {
            InitializeComponent();
        }

        public FoodOrderItem(string id, string count)
        {
            InitializeComponent();
            this.idLabel.Text = id;
            this.countLabel.Text = count;

            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from Dish where DishID = '" + id + "'";

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();

            while (reader.Read())
            {
                name = reader["Name"].ToString();
                price = Convert.ToDecimal(reader["Price"]);
                point = Convert.ToDecimal(reader["Point"]);
                
                // Retrieve image data as byte array
                byte[] imageData = (byte[])reader["Picture"];

                // Convert byte array to Image              
                using (MemoryStream ms = new MemoryStream(imageData))
                {
                    image = Image.FromStream(ms);
                }
            }
            reader.Close();

            this.nameLabel.Text = name;
            this.costLabel.Text = price.ToString();
            this.pointLabel.Text = point.ToString();
            this.pictureBox1.Image = image;
        }

        private void increaseBtn_Click(object sender, EventArgs e)
        {

        }

        private void FoodOrderItem_Load(object sender, EventArgs e)
        {

        }
    }
}

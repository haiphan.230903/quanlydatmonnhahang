﻿CREATE TABLE Restaurant
(
	Name VARCHAR(30) PRIMARY KEY,
	Address VARCHAR(50),
);

CREATE TABLE Employee 

(	
	EmployeeID		CHAR(9)		PRIMARY KEY,
	Fullname VARCHAR (30),
	Sex		CHAR(1),
	Email VARCHAR (30),
	Password VARCHAR(20),
	HouseNumber VARCHAR (5),
	Street VARCHAR (30),
	CommuneWard VARCHAR(30),
	District VARCHAR (30),
	City VARCHAR(30),
	Salary	DECIMAL(10,2),
	EmployeeStatus VARCHAR(10),
	StartDate	DATE,
	EndDate DATE,
	SupervisorID CHAR(9),
	RestaurantName VARCHAR(30),

	CONSTRAINT 	Fk_Employee_Restaurant_Name   FOREIGN KEY (RestaurantName) 
				REFERENCES Restaurant(Name)
				ON UPDATE CASCADE,
	
	CONSTRAINT 	Fk_Employee_SupervisorID   FOREIGN KEY (SupervisorID) 
				REFERENCES Employee(EmployeeID)
);
go 
CREATE FUNCTION dbo.CheckSalaryConstraint (
    @EmployeeID CHAR(9),
    @Salary DECIMAL(10,2)
)
RETURNS BIT
AS
BEGIN
    DECLARE @SupervisorSalary DECIMAL(10,2);

    -- Get the supervisor's salary
    SELECT @SupervisorSalary = Salary
    FROM Employee
    WHERE EmployeeID = @EmployeeID;

    -- Check the salary constraint
    RETURN CASE WHEN @Salary > @SupervisorSalary THEN 0 ELSE 1 END;
END;
go
ALTER TABLE Employee
ADD CONSTRAINT CHK_SalaryConstraint
CHECK (dbo.CheckSalaryConstraint(EmployeeID, Salary) = 1);






CREATE TABLE Dish
(	
	DishID	CHAR(9)	PRIMARY KEY,
	Name	VARCHAR(20),
	Description VARCHAR (40),
	Type VARCHAR(15),
	Price VARCHAR(4),
	Point DECIMAL(2,1),
	Picture Image,
			
);

CREATE TABLE Orders
(	
	OrderID	CHAR(9)	PRIMARY KEY,
	Status	VARCHAR(20),
	OrderTime datetime,
	Note VARCHAR(20),
	OrderType VARCHAR(10),
	TotalPoint DECIMAL(5,1),
	TotalPrice DECIMAL(10,1)
);

CREATE TABLE Promotion
(
	Code CHAR(9) PRIMARY KEY,
	Description VARCHAR(20),
	RequiredPoint VARCHAR(2),
	DiscountPercent Decimal (4,1),
	StartDate	DATE,
	EndDate DATE,
);

CREATE TABLE Customer

(	
	CustomerID		CHAR(9)		PRIMARY KEY,
	Fullname VARCHAR (30),
	Sex		CHAR(1),
	Email VARCHAR (30),
	Pasword VARCHAR(20),
	HouseNumber VARCHAR (5),
	Street VARCHAR (30),
	CommuneWard VARCHAR(30),
	District VARCHAR (30),
	City VARCHAR(30),

);

CREATE TABLE EmployeePhoneNumber
(
	EmployeeID CHAR(9),
	EmployeePhoneNum VARCHAR(12),
	PRIMARY KEY(EmployeeID, EmployeePhoneNum),

	CONSTRAINT 	Fk_PhoneNumber_Employee_EmployeeID   FOREIGN KEY (EmployeeID) 
				REFERENCES Employee(EmployeeID)
				ON UPDATE CASCADE,
);


CREATE TABLE CustomerPhoneNumber
(
	CustomerID CHAR(9),
	CustomerPhoneNum VARCHAR(12),
	PRIMARY KEY(CustomerID, CustomerPhoneNum),

	CONSTRAINT 	Fk_CustomerPhoneNumber_Customer_Employe  FOREIGN KEY (CustomerID) 
				REFERENCES Customer(CustomerID)
				ON UPDATE CASCADE,
);

CREATE TABLE Chef
(
	ChefID CHAR(9) PRIMARY KEY,
	Experience VARCHAR(10),

	CONSTRAINT 	Fk_Chef_Employee_ChefID   FOREIGN KEY (ChefID) 
				REFERENCES Employee(EmployeeID)
				ON UPDATE CASCADE,


);

CREATE TABLE Shipper
(
	ShipperID CHAR(9) PRIMARY KEY,
	

	
	CONSTRAINT 	Fk_Shipper_Employee_ShipperID   FOREIGN KEY (ShipperID) 
				REFERENCES Employee(EmployeeID)
				ON UPDATE CASCADE
				,

				
);


create table ShipperLicensePlate
(
	LicensePlate VARCHAR(10),
	ShipperID CHAR(9),
	primary key (LicensePlate, ShipperID),

	CONSTRAINT 	Fk_ShipperPlate_Shipper_ID   FOREIGN KEY (ShipperID) 
				REFERENCES Shipper(ShipperID)
				ON UPDATE CASCADE
);

create table Delivery
(
	OrderID CHAR(9) PRIMARY KEY,
	ShipperID CHAR(9),
	ShippingAddress VARCHAR(50),

	CONSTRAINT 	Fk_Delivery_Shipper_ShipperID   FOREIGN KEY (ShipperID) 
				REFERENCES Shipper(ShipperID)
				ON UPDATE CASCADE,

	CONSTRAINT 	Fk_Delivery_Order_OrderID   FOREIGN KEY (OrderID) 
				REFERENCES Orders(OrderID)
				ON UPDATE CASCADE

);

CREATE TABLE Steward
(
	StewardID CHAR(9) PRIMARY KEY,
	
	
	CONSTRAINT 	Fk_Steward_Employee_StewardID   FOREIGN KEY (StewardID) 
				REFERENCES Employee(EmployeeID)
				ON UPDATE CASCADE
				,

	
		
);

CREATE TABLE WorkingShift
(
	EmployeeID Char(9),
	StartTime	time,
	EndTime time,
	WorkingDay Date,

	PRIMARY KEY (EmployeeID, StartTime, EndTime, WorkingDay),
	CONSTRAINT 	Fk_WorkingShift_Employee_EmployeeID   FOREIGN KEY (EmployeeID) 
				REFERENCES Employee(EmployeeID)
				ON UPDATE CASCADE
				,
);

CREATE TABLE RestaurantTable
(
	TableID CHAR(3) PRIMARY KEY,
	Slot CHAR(1),
	RestaurantName VARCHAR(30),
	Status VARCHAR(20),

	CONSTRAINT 	Fk_Table_Restaurant_Name   FOREIGN KEY (RestaurantName) 
				REFERENCES Restaurant(Name)
				ON UPDATE CASCADE
				,
);

CREATE TABLE DishInOrder
(
	OrderID CHAR(9),
	DishID CHAR(9),
	PRIMARY KEY(OrderID, DishID),
	NumberOfDish DECIMAL(3,0)

	CONSTRAINT 	Fk_DishInOrder_Order_ID   FOREIGN KEY (OrderID) 
				REFERENCES Orders(OrderID)
				ON UPDATE CASCADE
				,
	CONSTRAINT 	Fk_DishInOrder_Dish_ID   FOREIGN KEY (DishID) 
				REFERENCES Dish(DishID)
				ON UPDATE CASCADE
				,
);

CREATE TABLE CustomerRequestTable
(
	TableID CHAR(3),

	CustomerID Char(9),
	Primary key(TableID, CustomerID),

	CONSTRAINT 	Fk_CustomerRequestTable_Table_ID   FOREIGN KEY (TableID) 
				REFERENCES RestaurantTable(TableID)
				ON UPDATE CASCADE
				,

	CONSTRAINT 	Fk_CustomerRequestTable_Customer_ID   FOREIGN KEY (CustomerID) 
				REFERENCES Customer(CustomerID)
				ON UPDATE CASCADE
				,
);

CREATE TABLE CustomerOrdersAtRestaurant
(
	OrderID CHAR(9) PRIMARY KEY,
	CustomerID Char(9),
	RestaurantName VARCHAR(30),
	

	CONSTRAINT 	Fk_CustomerOrders_Orders_ID   FOREIGN KEY (OrderID) 
				REFERENCES Orders(OrderID)
				ON UPDATE CASCADE
				,

	CONSTRAINT 	Fk_CustomerOrders_Customer_ID   FOREIGN KEY (CustomerID) 
				REFERENCES Customer(CustomerID)
				ON UPDATE CASCADE
				,
	CONSTRAINT 	Fk_CustomerOrders_Restaurant_Name   FOREIGN KEY (RestaurantName) 
				REFERENCES Restaurant(Name)
				ON UPDATE CASCADE
);

CREATE TABLE OrderTimes 
(
	TableID CHAR(3),
	CustomerID CHAR(9),
	OrderDate	DATE,
	OrderTime	TIME,
	Slot	Decimal(1,0)
	Primary key(TableID, CustomerID, OrderDate, OrderTime, Slot)

	CONSTRAINT 	Fk_OrderTimes_Table_ID   FOREIGN KEY (TableID) 
				REFERENCES RestaurantTable(TableID)
				ON UPDATE CASCADE
				,

	CONSTRAINT 	Fk_OrderTimes_Customer_ID   FOREIGN KEY (CustomerID) 
				REFERENCES Customer(CustomerID)
				ON UPDATE CASCADE
				,
);

CREATE TABLE Review
(
	CustomerID CHAR(9),
	RestaurantName VARCHAR(30),
	ReviewPoint Decimal(2,1),
	ReviewComment Varchar(30),
	
	primary key(CustomerID, RestaurantName),

	CONSTRAINT 	Fk_Review_Customer_ID   FOREIGN KEY (CustomerID) 
				REFERENCES Customer(CustomerID)
				ON UPDATE CASCADE
				,

	CONSTRAINT 	Fk_Review_Restaurant_Name   FOREIGN KEY (RestaurantName) 
				REFERENCES Restaurant(Name)
				ON UPDATE CASCADE
				,

);

CREATE TABLE Voucher
(
	PromotionCode CHAR(9),
	VoucherCode CHAR(9),
	CustomerID CHAR(9),
	OrderID CHAR(9),
	PRIMARY KEY(PromotionCode, VoucherCode),

	CONSTRAINT 	Fk_Voucher_Promotion_Code   FOREIGN KEY (PromotionCode) 
				REFERENCES Promotion(Code)
				ON UPDATE CASCADE
				,
	CONSTRAINT 	Fk_Voucher_Orders_ID   FOREIGN KEY (OrderID) 
				REFERENCES Orders(OrderID)
				ON UPDATE CASCADE
				,

);
go
CREATE FUNCTION Top3FavoriteFood(@StartDate DATE, @EndDate DATE)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP 3 DishID, SUM(NumberOfDish) AS TotalOrderedAmount
    FROM DishInOrder dio
    JOIN Orders o ON dio.OrderID = o.OrderID
    WHERE OrderTime BETWEEN @StartDate AND @EndDate
    GROUP BY DishID
    ORDER BY TotalOrderedAmount DESC
);
go


-- Insert data into Restaurant table
INSERT INTO Restaurant VALUES 
('Food Paradise', '123 Main St'),
('Taste of Italy', '456 Oak St'),
('Sushi Heaven', '789 Elm St'),
('Burger Joint', '101 Maple St'),
('Mexican Fiesta', '202 Pine St');



-- Insert data into Employee table
INSERT INTO Employee VALUES 
('E00000001', 'John Doe', 'M', 'john.doe@example.com', '12345678', '123', 'Main St', 'Green Haven', 'Emerald', 'Aurora City',  50000.00, 'Active', '2023-01-01', NULL, NULL, 'Food Paradise'),
('E00000002', 'Jane Smith', 'F', 'jane.smith@example.com', '123456789', '456', 'Oak St', 'Harmony Springs', 'Azure Heights', 'Havenburg',  60000.00, 'Active', '2023-02-01', NULL, 'E00000001', 'Food Paradise'),
('E00000003', 'Bob Johnson', 'M', 'bob.johnson@example.com', 'johnsonhandsome', '789', 'Elm St', 'Sunflower Meadows', 'Golden Gate', 'Crestwood City',  55000.00, 'Active', '2023-03-01', NULL, NULL, 'Taste of Italy'),
('E00000004', 'Emily White', 'F', 'emily.white@example.com', 'emilyinparis', '101', 'Maple St', 'Riverstone', 'Silverlake', 'Rivertown',  52000.00, 'Active', '2023-04-01', NULL, 'E00000003', 'Sushi Heaven'),
('E00000005', 'Alex Brown', 'M', 'alex.brown@example.com', 'brownchocolate', '202', 'Pine St', 'Serenity Valley', 'Harmony Hills', 'Horizon Springs',  48000.00, 'Active', '2023-05-01', NULL, 'E00000003', 'Burger Joint');

-- Insert data into Dish table

Insert Dish
   (DishID, Name, 	Description,
	Type ,
	Price ,
	Point ,
	Picture ) 
   Select 'D00000001', 'Spaghetti Bolognese','Classic Italian dish', 'Pasta', '12', 4.5, BulkColumn 
   from Openrowset( Bulk 'E:\HK231\Database\Image\spaghetti-bolognese.jpg', Single_Blob) as Picture;


Insert Dish
   (DishID, Name, 	Description,
	Type ,
	Price ,
	Point ,
	Picture ) 
   Select 'D00000002', 'Chicken Caesar Salad', 'Fresh and healthy salad', 'Salad', '9', 4.0, BulkColumn 
   from Openrowset( Bulk 'E:\HK231\Database\Image\ChickenCaesarSalad.jpg', Single_Blob) as Picture;

Insert Dish
   (DishID, Name, 	Description,
	Type ,
	Price ,
	Point ,
	Picture ) 
   Select 'D00000003', 'Sushi Combo', 'Assorted sushi rolls', 'Sushi', '18', 4.2, BulkColumn 
   from Openrowset( Bulk 'E:\HK231\Database\Image\Sushi.jpg', Single_Blob) as Picture;

   Insert Dish
   (DishID, Name, 	Description,
	Type ,
	Price ,
	Point ,
	Picture ) 
   Select 'D00000004', 'Classic Burger', 'Juicy beef patty with cheese', 'Burger', '8', 4.7, BulkColumn 
   from Openrowset( Bulk 'E:\HK231\Database\Image\Burger.jpg', Single_Blob) as Picture;

   Insert Dish
   (DishID, Name, 	Description,
	Type ,
	Price ,
	Point ,
	Picture ) 
   Select 'D00000005', 'Tacos', 'Mexican street food delight', 'Mexican', '10', 4.4, BulkColumn 
   from Openrowset( Bulk 'E:\HK231\Database\Image\Tacos.jpg', Single_Blob) as Picture;


-- Insert data into Orders table
INSERT INTO Orders VALUES 
('O00000001', 'Pending', '2023-01-15 12:30:00', 'None', 'Dine-In'),
('O00000002', 'Completed', '2023-02-10 18:45:00', 'Extra spicy', 'Delivery'),
('O00000003', 'Completed', '2023-03-05 20:00:00', 'No onions', 'Delivery'),
('O00000004', 'Pending', '2023-04-20 15:00:00', 'No nuts', 'Dine-In'),
('O00000005', 'Completed', '2023-05-12 14:30:00', 'Gluten-free', 'Delivery');

-- Insert data into Promotion table
INSERT INTO Promotion VALUES 
('P00000001', '20% Off', '50', 20.0, '2023-01-01', '2023-01-31'),
('P00000002', 'Free Dessert', '30', 0.0, '2023-02-01', '2023-02-28'),
('P00000003', 'Half Price Sunday', '40', 50.0, '2023-03-01', '2023-03-31'),
('P00000004', '10% Off', '20', 10.0, '2023-04-01', '2023-04-30'),
('P00000005', 'Happy Hour', '10', 15.0, '2023-05-01', '2023-05-31');

-- Insert data into Customer table
INSERT INTO Customer VALUES 
('C00000001', 'Alice Johnson', 'F', 'alice.johnson@example.com', 'pass123', '789', 'Maple St', 'Meadowview', 'Sapphire Shores', 'Radiance Metropolis'),
('C00000002', 'Bob Miller', 'M', 'bob.miller@example.com', 'secure456', '101', 'Cedar St', 'Radiant Fields ', 'Harmony Hills', 'Maplewood City'),
('C00000003', 'Eva Davis', 'F', 'eva.davis@example.com', 'eva789', '303', 'Willow St', 'Whispering Oaks', 'Starlight Ridge', 'Summitville' ),
('C00000004', 'David Lee', 'M', 'david.lee@example.com', 'david456', '505', 'Palm St', 'Blissful Grove', 'Moonlight Grove', 'Phoenix Falls'),
('C00000005', 'Grace Smith', 'F', 'grace.smith@example.com', 'gracepass', '707', 'Oak St', 'Serenity Valley', 'Sunrise Meadows ', 'Horizon Springs');

-- Insert data into EmployeePhoneNumber table
INSERT INTO EmployeePhoneNumber VALUES 
('E00000001', '123-456-7890'),
('E00000002', '987-654-3210'),
('E00000003', '111-222-3333'),
('E00000004', '444-555-6666'),
('E00000005', '777-888-9999');

-- Insert data into CustomerPhoneNumber table
INSERT INTO CustomerPhoneNumber VALUES 
('C00000001', '555-123-4567'),
('C00000002', '555-987-6543'),
('C00000003', '555-111-2222'),
('C00000004', '555-444-5555'),
('C00000005', '555-777-8888');

-- Insert data into Chef table
INSERT INTO Chef VALUES 
('E00000001', '8 years'),
('E00000002', '10 years');

-- Insert data into Shipper table
INSERT INTO Shipper VALUES 
('E00000003'),
('E00000004');

-- Insert data into ShipperLicensePlate table
INSERT INTO ShipperLicensePlate VALUES 
('7AAXS8', 'E00000003'),
('6TRJ24', 'E00000004');

-- Insert data into Delivery table
INSERT INTO Delivery VALUES 
('O00000001', NULL, NULL),
('O00000002', 'E00000003', '456 Oak St, Townsville'),
('O00000003', 'E00000004', '789 Elm St, Big City'),
('O00000004', NULL, NULL),
('O00000005', 'E00000004', '202 Pine St, Metropolis');

-- Insert data into Steward table
INSERT INTO Steward VALUES 
('E00000005');

-- Insert data into WorkingShift table
INSERT INTO WorkingShift VALUES 
('E00000001', '09:00:00', '17:00:00', '2023-01-01'),
('E00000002', '12:00:00', '20:00:00', '2023-02-01'),
('E00000003', '08:00:00', '16:00:00', '2023-03-01'),
('E00000004', '14:00:00', '22:00:00', '2023-04-01'),
('E00000005', '10:00:00', '18:00:00', '2023-05-01');

-- Insert data into RestaurantTable table
INSERT INTO RestaurantTable VALUES 
('001', '3', 'Food Paradise', 'Occupied'),
('002', '3', 'Food Paradise', 'Occupied'),
('003', '3', 'Food Paradise', 'Occupied'),
('004', '4', 'Taste of Italy', 'Available'),
('005', '4', 'Taste of Italy', 'Available'),
('006', '4', 'Taste of Italy', 'Available'),
('007', '3', 'Sushi Heaven', 'Occupied'),
('008', '3', 'Sushi Heaven', 'Occupied'),
('009', '3', 'Sushi Heaven', 'Occupied'),
('010', '2', 'Burger Joint', 'Available'),
('011', '2', 'Burger Joint', 'Available'),
('012', '3', 'Mexican Fiesta', 'Occupied');

-- Insert data into DishInOrder table
INSERT INTO DishInOrder VALUES 
('O00000001', 'D00000001', 2),
('O00000002', 'D00000002', 1),
('O00000003', 'D00000003', 3),
('O00000004', 'D00000004', 1),
('O00000005', 'D00000005', 2);

-- Insert data into CustomerRequestTable table
INSERT INTO CustomerRequestTable VALUES 
('001', 'C00000001'),
('002', 'C00000001'),
('002', 'C00000002'),
('003', 'C00000003'),
('004', 'C00000004'),
('005', 'C00000005');



-- Insert data into CustomerOrdersAtRestaurant table
INSERT INTO CustomerOrdersAtRestaurant VALUES 
('O00000001', 'C00000001', 'Food Paradise'),
('O00000002', 'C00000002', 'Taste of Italy'),
('O00000003', 'C00000003', 'Sushi Heaven'),
('O00000004', 'C00000004', 'Burger Joint'),
('O00000005', 'C00000005', 'Mexican Fiesta');

-- Insert data into OrderTimes table
INSERT INTO OrderTimes VALUES 
('001', 'C00000001', '2023-01-15', '12:30:00', 1),
('001', 'C00000001', '2023-01-18', '12:30:00', 1),
('002', 'C00000001', '2023-01-21', '12:30:00', 1),
('002', 'C00000002', '2023-02-10', '18:45:00', 2),
('003', 'C00000003', '2023-03-05', '20:00:00', 1),
('004', 'C00000004', '2023-04-20', '15:00:00', 2),
('005', 'C00000005', '2023-05-12', '14:30:00', 1);

-- Insert data into Review table
INSERT INTO Review VALUES 
('C00000001', 'Food Paradise', 4.5, 'Great experience!'),
('C00000002', 'Taste of Italy', 3.8, 'Good food, slow service'),
('C00000003', 'Sushi Heaven', 4.0, 'Excellent sushi selection'),
('C00000004', 'Burger Joint', 4.7, 'Best burgers in town'),
('C00000005', 'Mexican Fiesta', 4.2, 'Authentic Mexican flavors');

-- Insert data into Voucher table
INSERT INTO Voucher VALUES 
('P00000001', 'V00000001', 'C00000001', 'O00000001'),
('P00000002', 'V00000002', 'C00000002', 'O00000002'),
('P00000003', 'V00000003', 'C00000003', 'O00000003'),
('P00000004', 'V00000004', 'C00000004', 'O00000004'),
('P00000005', 'V00000005', 'C00000005', 'O00000005');

go
CREATE FUNCTION Top3FavoriteFood(@StartDate DATE, @EndDate DATE)
RETURNS TABLE
AS
RETURN
(
    SELECT TOP 3 DishID, SUM(NumberOfDish) AS TotalOrderedAmount
    FROM DishInOrder dio
    JOIN Orders o ON dio.OrderID = o.OrderID
    WHERE OrderTime BETWEEN @StartDate AND @EndDate
    GROUP BY DishID
    ORDER BY TotalOrderedAmount DESC
);
go

select * from Orders

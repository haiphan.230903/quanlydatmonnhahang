﻿namespace QuanLyDatMonNhaHangApp
{
    partial class Sushimax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sushimaxlayout = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // sushimaxlayout
            // 
            this.sushimaxlayout.BackColor = System.Drawing.Color.White;
            this.sushimaxlayout.Location = new System.Drawing.Point(28, 28);
            this.sushimaxlayout.Name = "sushimaxlayout";
            this.sushimaxlayout.Size = new System.Drawing.Size(968, 767);
            this.sushimaxlayout.TabIndex = 0;
            this.sushimaxlayout.Paint += new System.Windows.Forms.PaintEventHandler(this.sushimaxlayout_Paint);
            // 
            // Sushimax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 843);
            this.Controls.Add(this.sushimaxlayout);
            this.Name = "Sushimax";
            this.Text = "Sushimax";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel sushimaxlayout;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyDatMonNhaHangApp
{
    public partial class Orders : Form
    {
        string strCon = @"Data Source=ADMIN;Initial Catalog=NewQLDMNH;Integrated Security=True";
        SqlConnection sqlCon = null;
        public Orders()
        {
            InitializeComponent();
            try
            {
                LoadOrder();
            }
            catch (Exception ex)
            {
                AnnounceForm announceForm = new AnnounceForm();
                announceForm.annouceText.Text = ex.Message;
                announceForm.Show();
                announceForm.BringToFront();
            }
        }

        public void LoadOrder()
        {
            try
            {
                sqlCon = new SqlConnection(strCon);
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                    //MessageBox.Show("Ket noi thanh cong");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select * from PaidOrdersWithSushi";

            //SqlCommand sqlCmd = new SqlCommand("PaidOrdersWithSushi", sqlCon);
            //sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Connection = sqlCon;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            orderListFlow.Controls.Clear();
            while (reader.Read())
            {
                string orderId = reader["OrderID"].ToString();
                OrderItem orderItem = new OrderItem();
                orderItem.LoadOrder("'" + orderId + "'");
                orderListFlow.Controls.Add(orderItem);
            }
            reader.Close();
        }

        private void Orders_Load(object sender, EventArgs e)
        {

        }

        private void orderListFlow_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
